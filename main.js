class PromisePolling{

	constructor(onPoll, delay, timeout){
  	this.onPoll = onPoll
    this.delay = delay
    this.timeout = timeout
    this.timer = null
    this.timeoutTimer = null
     initPromise()
  }
  
  initPromise(){
    this.promise = new Promise((reject, resolve) => {
    	this.reject = reject
      this.resolve = resolve
    }).then(() => {
    	clearTimeout(this.timeoutTimer)
      this.timeoutTimer = null
    })
  }
  
  run(){
  	this.setTimeout()
  	this.timer = setInterval(() => {
    	this.onPoll(resolve, reject)
    }, this.delay)
  	return this.promise
  }
  
  setTimeout(){
  	this.timeoutTimer = setTimeout(() => {
    	clearInterval(this.timer)
      this.timer = null
      this.reject(new Error('Timeout'))
    }, this.timeout);
  }

}